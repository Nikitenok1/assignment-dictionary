global _start

%include "words.inc"
%include "lib.inc"
extern find_word

%define STR256STACKALLOC 256
%define STDOUT 1
%define STDERR 0

section .rodata

enter_word: db "A word to search < ", 0
input_too_long: db "Key entered is too long", 0
word_not_found: db "Word was not found", 0

section .text

_start:
    mov rdi, enter_word
    mov rsi, STDOUT
    call print_string
    sub rsp, STR256STACKALLOC
    mov rdi, rsp
    mov rsi, STR256STACKALLOC
    call read_line

    test rax, rax
    je .inp_too_long

    mov rdi, rax
    mov rsi, list_pointer
    call find_word

    test rax, rax
    je .inp_not_found

    add rax, 8
    mov rdi, rax
    call string_length
    inc rdi
    add rdi, rax

    mov rsi, STDOUT
    jmp .print_message_and_exit

.inp_too_long:
    mov rdi, input_too_long
    mov rsi, STDERR
    jmp .print_message_and_exit

.inp_not_found:
    mov rdi, word_not_found
    mov rsi, STDERR

.print_message_and_exit:
    call print_string
    call print_newline
    call exit



