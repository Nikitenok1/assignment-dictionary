ASM = nasm
ASMFLAGS = -felf64
LD = ld

.DEFAULT_GOAL = main

main.o: main.asm lib.inc colon.inc words.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

main: main.o lib.o dict.o
	$(LD) -o $@ $^

.PHONY: clean
clean:
	rm main *.o
