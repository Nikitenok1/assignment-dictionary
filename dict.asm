section .text

global find_word

%include "lib.inc"

%define ADDRSIZE 8

; принимает два аргумента:
; Указатель на нуль-терминированную строку (rdi)
; Указатель на начало словаря (rsi)
; find_word пройдёт по всему словарю в поисках подходящего ключа.
; Если подходящее вхождение найдено, вернёт адрес начала вхождения в словарь (rax) (не значения),
; иначе вернёт 0 (rax).
; mod: rsi, rax
find_word:
    push rsi
    push rdi
    add rsi, ADDRSIZE
    call string_equals
    pop rdi
    pop rsi
    test rax, rax
    jne .end
    mov rsi, [rsi]
    test rsi, rsi
    jne find_word
    ret             ; дошли до конца и не нашли слово - выходим (в rax уже 0)
.end:
    mov rax, rsi
    ret