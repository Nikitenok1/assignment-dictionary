%define list_pointer 0

%macro colon 2
    %ifid %2
        %2: dq list_pointer
        %define list_pointer %2

        db %1, 0
    %endif
%endmacro
