section .text

global exit
global string_length
global print_string
global print_newline
global print_char
global print_int
global print_uint
global string_equals
global read_char
global read_word
global read_line
global parse_uint
global parse_int
global string_copy

%define EXIT 60
%define HEX2CHAR 0x30
%define READ 0
%define STDIN 0
%define STDOUT 1
%define UINT8STACKALLOC 22
%define WRITE 1

; Принимает код возврата (rdi) и завершает текущий процесс
exit:
    mov rax, EXIT
    syscall

; Принимает указатель на нуль-терминированную строку (rdi), возвращает её длину (rax)
; mod: rax
string_length:
    xor rax, rax
.iter:
    cmp byte[rdi+rax], 0
    je .end
    inc rax
    jmp .iter
.end:
    ret

; Принимает указатель на нуль-терминированную строку (rdi), выводит её в stdout
; mod: rax, rdx, rsi, rdi
print_string:
    push rdi
    push rsi
    call string_length
    mov rdx, rax
    mov rax, WRITE
    pop rdi
    pop rsi
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
; mod: rdi, rax, rdx, rsi
print_newline:
    mov rdi, 0xA

; Принимает код символа (rdi) и выводит его в stdout
; mod: rax, rdx, rsi
print_char:
    mov rdx, 1
    push rdi
    mov rsi, rsp
    mov rdi, STDOUT
    mov rax, WRITE
    syscall
    pop rdi
    ret

; Выводит знаковое 8-байтовое число (rdi) в десятичном формате
; mod: rax, r8, r9, rdx, rdi, rsi
print_int:
    test rdi, rdi
    jns print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi

; Выводит беззнаковое 8-байтовое число (rdi) в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
; mod: rax, r8, r9, rdx, rdi, rsi
print_uint:
    mov rax, rdi
    mov r8, rsp
    mov r9, 0xA
    sub rsp, UINT8STACKALLOC
    dec r8
    mov byte[r8], 0
.iter:
    dec r8
    xor rdx, rdx
    div r9
    add rdx, HEX2CHAR
    mov byte[r8], dl
    test rax, rax
    jne .iter
    mov rdi, r8
    call print_string
    add rsp, UINT8STACKALLOC
    ret

; Принимает два указателя на нуль-терминированные строки (rdi), (rsi), возвращает (rax) 1 если они равны, 0 иначе
; mod: r8, rax
string_equals:
    xor r8, r8
    xor rax, rax
.iter:
    mov al, byte[rdi+r8]
    cmp al, byte[rsi+r8]
    jne .not_equal
    inc r8
    test al, al
    jne .iter
    mov rax, 1
    ret
.not_equal:
    xor rax, rax
    ret

; Читает один символ из stdin и возвращает его (rax). Возвращает 0 если достигнут конец потока
; mod: rsi, rdx, rax, rdi
read_char:
    push 0
    mov rsi, rsp
    mov rdx, 1
    mov rax, READ
    mov rdi, STDIN
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера (rdi), размер буфера (rsi)
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в (rax), длину слова в (rdx).
; При неудаче возвращает 0 в (rax)
; Эта функция должна дописывать к слову нуль-терминатор
; mod: r8, r9, r10, rax, rdx
read_word:
    dec rsi
    xor r8, r8  ; a counter
.iter:
    push rdi
    push rsi
    push r8
    call read_char
    pop r8
    pop rsi
    pop rdi
    cmp rsi, r8
    je .overflow
    cmp rax, ' '
    je .ifstart
    cmp rax, '\n'
    je .ifstart
    cmp rax, 0x9
    je .ifstart
    cmp rax, 0
    je .correct
    mov byte[rdi+r8], al
    inc r8
    jmp .iter
.ifstart:
    test r8, r8
    je .iter
.correct:
    mov byte[rdi+r8], 0
    mov rax, rdi
    mov rdx, r8
    jmp .end
.overflow:
    xor rax, rax
.end:
    ret

; Принимает: адрес начала буфера (rdi), размер буфера (rsi)
; При успехе возвращает адрес буфера в (rax), длину слова в (rdx).
; При неудаче возвращает 0 в (rax)
read_line:
    mov r10, rdi; to prevent too much pushs and pops
    mov r9, rsi ; remaining buffer size
    dec r9
    xor r8, r8  ; a counter
.iter:
    call read_char
    cmp r9, r8
    je .overflow
    cmp rax, 0xA
    je .correct
    test rax, rax
    je .correct
    mov byte[r10+r8], al
    inc r8
    jmp .iter
.correct:
    mov byte[r10+r8], 0
    mov rax, r10
    mov rdx, r8
    jmp .end
.overflow:
    xor rax, rax
.end:
    ret

; Принимает указатель на строку (rdi), пытается
; прочитать из её начала беззнаковое число.
; Возвращает в (rax): число, (rdx) : его длину в символах
; (rdx) = 0 если число прочитать не удалось
; mod: rdx, rax, r8
parse_uint:
    xor rdx, rdx
    xor rax, rax
    xor r8, r8
.iter:
    mov r8b, byte[rdi+rdx]
    cmp r8, '9'
    jg .nan
    cmp r8, '0'
    jl .nan
    inc rdx
    sub r8, '0'
    imul rax, 10
    add rax, r8
    jmp .iter
.nan:
    ret

; Принимает указатель на строку (rdi), пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
; mod: rdx, rax, r8, rdi
parse_int:
    xor r8, r8
    mov r8b, byte[rdi]
    cmp r8, '-'
    jne parse_uint
    inc rdi
    call parse_uint
    test rdx, rdx
    je .end
    neg rax
    inc rdx
.end:
    ret

; Принимает указатель на строку (rdi), указатель на буфер (rsi) и длину буфера (rdx)
; Копирует строку в буфер
; Возвращает длину строки (rax) если она умещается в буфер, иначе 0
; mod: rax, r8
string_copy:
    xor rax, rax
    xor r8, r8
.iter:
    cmp rax, rdx
    je .overflow
    mov r8b, byte[rdi+rax]
    mov byte[rsi+rax], r8b
    test r8b, r8b
    inc rax
    jmp .iter
.overflow:
    xor rax, rax
.end:
    ret